$(function(){
    		$("[data-toggle='tooltip']").tooltip();
    		$("[data-toggle='popover']").popover();
    		$('.carousel').carousel({interval: 2000 });

    		$('#reservar').on('show.bs.modal', function(e){
    			console.log('El modal reservar esta activo');
    			$('#reservabtn').removeClass('btn-primary');
    			$('#reservabtn').addClass('btn-light');
    			$('#reservabtn').prop('disabled',true);
    		});
    		$('#reservar').on('shown.bs.modal', function(e){
    			console.log('El modal reservar esta inactivo');
    		});
    		$('#reservar').on('hide.bs.modal', function(e){
    			console.log('El modal reservar se oculta');
    		});
    		$('#reservar').on('hidden.bs.modal', function(e){
    			console.log('El modal reservar se oculto');
    			$('#reservabtn').prop('disabled',false);
    		});
    	});